<?php
namespace App\Traits;

use function App\Helpers\tenant;
use Illuminate\Database\Eloquent\Builder;

trait Tenantable
{
    public static function bootTenantable() {
        if (auth()->check()) {
            static::creating(function ($model) {
                $model->tenant_id = tenant()->id();
            });
        }

        if (auth()->check()) {
            static::addGlobalScope('tenant_id', function (Builder $builder) {
                $builder->where('tenant_id', '=', tenant()->id());
            });
        }
    }
}

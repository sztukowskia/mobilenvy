<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ApiForwarder {

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $accessKey;

    public function __construct(string $baseUri, string $accessKey)
    {
        $this->client = new Client([
            'base_uri' => $baseUri
        ]);

        $this->accessKey = $accessKey;
    }

    public function forward(Request $request, string $endpoint, array $getParams = null) {
        $request->headers->remove('Content-Type');
        $response = $this->client->request($request->method(), $this->makeUrl($endpoint, $getParams), [
            'headers' => $request->headers->all(),
            'form_params' => $request->all(),
            'http_errors' => false,
        ]);

        return response($response->getBody(), $response->getStatusCode(), $response->getHeaders());
    }

    private function makeUrl(string $endpoint, array $getParams = null): string {
        $endpoint .= '?ACCESS_KEY='.$this->accessKey;

        if (is_array($getParams)) {
            foreach ($getParams as $key => $value) {
                $endpoint .= "&{$key}={$value}";
            }
        }

        return $endpoint;
    }
}

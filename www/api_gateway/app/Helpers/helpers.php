<?php
namespace App\Helpers;

use App\Models\Tenant;

if (!function_exists('tenant')) {
    function tenant(): Tenant {
        if (auth()->check()) {
            return Tenant::where(Tenant::OWNER_ID, auth()->id())->first();
        }
        return null;
    }
};

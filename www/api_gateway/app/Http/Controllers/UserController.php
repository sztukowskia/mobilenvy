<?php

namespace App\Http\Controllers;

use App\Services\ApiForwarder;
use Illuminate\Http\Request;

class UserController extends Controller
{

    private $api;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->api = new ApiForwarder(env('USERS_URL'), env('USERS_ACCESS_KEY'));
    }

    public function forward(Request $request, string $endpoint) {
        return $this->api->forward($request, 'api/'.$endpoint);
    }
}

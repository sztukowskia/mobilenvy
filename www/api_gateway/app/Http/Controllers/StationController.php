<?php

namespace App\Http\Controllers;

use App\Services\ApiForwarder;
use Illuminate\Http\Request;

class StationController extends Controller
{

    private $api;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->api = new ApiForwarder(env('STATIONS_URL'), env('STATIONS_ACCESS_KEY'));
    }

    public function forward(Request $request, string $endpoint) {
        if ($request->method() === 'GET') {
            return $this->api->forward($request, 'api/'.$endpoint, $request->all());

        }
        return $this->api->forward($request, 'api/'.$endpoint);
    }

    public function measure(Request $request) {
        return $this->api->forward($request, 'api/measure/create');
    }
}

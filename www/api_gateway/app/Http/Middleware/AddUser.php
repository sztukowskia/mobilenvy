<?php

namespace App\Http\Middleware;

use App\Services\ApiForwarder;
use Closure;
use Illuminate\Http\Request;

class AddUser
{
    public function handle(Request $request, Closure $next)
    {
        $api = new ApiForwarder(env('USERS_URL'), env('USERS_ACCESS_KEY'));
        $method = $request->getMethod();
        $request->setMethod('POST');
        $response = $api->forward($request, 'api/user');


        if ($response->getStatusCode() !== 200) {
            return $response;
        }

        $request->request->remove('user');

        $request->setMethod($method);

        $request->request->add(['user' => (json_decode($response->content()))->id]);
        return $next($request);
    }
}

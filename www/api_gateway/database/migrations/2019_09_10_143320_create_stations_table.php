<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationsTable extends Migration
{
    const TABLE = 'stations';
    const TENANTS_TABLE = 'tenants';
    const TENANT_ID = 'tenant_id';
    const ID = 'id';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const ACTIVE = 'active';
    const ACCESS_KEY = 'access_key';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->integer(self::TENANT_ID, false, true);
                $table->increments(self::ID);
                $table->float(self::LATITUDE);
                $table->float(self::LONGITUDE);
                $table->boolean(self::ACTIVE);
                $table->string(self::ACCESS_KEY);
                $table->timestamps();

                $table->foreign(self::TENANT_ID)->references(self::TENANT_ID)->on(self::TENANTS_TABLE);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::TABLE);
        Schema::enableForeignKeyConstraints();
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTenantKeys extends Migration
{
    const ID = 'id';
    const TENANT_ID = 'tenant_id';
    const OWNER_ID = 'owner_id';

    const TENANTS_TABLE = 'tenants';
    const USERS_TABLE = 'users';
    const PASSWORD_RESETS_TABLE = 'user_password_resets';
    const EMAIL_TOKENS_TABLE = 'user_email_verification_tokens';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TENANTS_TABLE, function (Blueprint $table) {
            $table->foreign(self::OWNER_ID)->references(self::ID)->on(self::USERS_TABLE);
        });


        Schema::table(self::USERS_TABLE, function (Blueprint $table) {
            $table->foreign(self::TENANT_ID)->references(self::TENANT_ID)->on(self::TENANTS_TABLE);
        });

        Schema::table(self::PASSWORD_RESETS_TABLE, function (Blueprint $table) {
            $table->foreign(self::TENANT_ID)->references(self::TENANT_ID)->on(self::TENANTS_TABLE);
        });

        Schema::table(self::EMAIL_TOKENS_TABLE, function (Blueprint $table) {
            $table->foreign(self::TENANT_ID)->references(self::TENANT_ID)->on(self::TENANTS_TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::USERS_TABLE, function (Blueprint $table) {
            $table->dropForeign([self::TENANT_ID]);
        });

        Schema::table(self::PASSWORD_RESETS_TABLE, function (Blueprint $table) {
            $table->dropForeign([self::TENANT_ID]);
        });

        Schema::table(self::EMAIL_TOKENS_TABLE, function (Blueprint $table) {
            $table->dropForeign([self::TENANT_ID]);
        });

        Schema::table(self::TENANTS_TABLE, function (Blueprint $table) {
            $table->dropForeign([self::OWNER_ID]);
        });
    }
}

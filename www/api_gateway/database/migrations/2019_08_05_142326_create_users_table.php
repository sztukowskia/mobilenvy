<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    const TABLE = 'users';
    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const DESCRIPTION = 'description';
    const PASSWORD = 'password';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const IMAGE = 'image';
    const TENANT_ID = 'tenant_id';



    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->integer(self::TENANT_ID, false, true);
                $table->increments(self::ID);
                $table->string(self::NAME)->nullable()->default(null);
                $table->string(self::EMAIL)->unique();
                $table->timestamp(self::EMAIL_VERIFIED_AT)->nullable()->default(null);
                $table->string(self::PASSWORD);
                $table->string(self::IMAGE)->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}

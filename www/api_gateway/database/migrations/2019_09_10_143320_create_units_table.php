<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    const TABLE = 'station_units';
    const TENANTS_TABLE = 'tenants';
    const TENANT_ID = 'tenant_id';
    const ID = 'id';
    const UNIT = 'unit';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->integer(self::TENANT_ID, false, true);
                $table->increments(self::ID);
                $table->string(self::UNIT);
                $table->timestamps();

                $table->foreign(self::TENANT_ID)->references(self::TENANT_ID)->on(self::TENANTS_TABLE);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::TABLE);
        Schema::enableForeignKeyConstraints();
    }
}

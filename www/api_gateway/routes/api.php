<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['cors', 'json'])->group(function() {
    /**
     * Users endpoints
     */
    Route::prefix('users')->group(function () {
        Route::any('/{endpoint}', 'UserController@forward')
            ->where('endpoint', '(.*)');
    });

    /**
     * Station endpoints
     */
    Route::middleware(['add_user'])->group(function () {
        Route::prefix('stations')->group(function () {
            Route::any('/{endpoint}', 'StationController@forward')
                ->where('endpoint', '(.*)');;
        });
    });

    Route::post('measure/create', 'StationController@measure');
});

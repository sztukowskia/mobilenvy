<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/refresh', 'Auth\LoginController@refreshToken');
Route::post('/register', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login');

Route::middleware(['jwt.auth'])->group(function () {
    Route::post('/logout', 'Auth\LoginController@logout');
    Route::post('/user', function () {
        return auth()->user();
    });
});

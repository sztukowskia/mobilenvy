<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTokensTable extends Migration
{
    const TABLE = 'user_email_verification_tokens';
    const USERS_TABLE = 'users';
    const ID = 'id';
    const USER_ID = 'user_id';
    const TOKEN = 'token';
    const EXPIRES_AT = 'expires_at';
    const TENANT_ID = 'tenant_id';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->integer(self::TENANT_ID, false, true);
                $table->increments(self::ID);
                $table->integer(self::USER_ID, false, true);
                $table->string(self::TOKEN, 32);
                $table->dateTime(self::EXPIRES_AT);
                $table->timestamps();

                $table->foreign(self::USER_ID)->references(self::ID)->on(self::USERS_TABLE);
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::TABLE);
        Schema::enableForeignKeyConstraints();
    }
}

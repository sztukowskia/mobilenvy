<?php

namespace App\Listeners;


use App\Events\Registered;
use App\Mail\RegisterMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class UserRegisteredListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(Registered $event)
    {
        Mail::to($event->user->getEmail())->send(new RegisterMail($event->user->getEmail(), $event->activationToken));
    }
}

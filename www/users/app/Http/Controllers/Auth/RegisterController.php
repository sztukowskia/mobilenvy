<?php

namespace App\Http\Controllers\Auth;

use App\Events\Registered;
use function App\Helpers\tenant;
use App\Models\EmailToken;
use App\Models\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'numeric']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'name' => null,
            'tenant_id' => tenant()->id()
        ]);

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        do {
            $emailVerifyToken = Str::random(32);
            $exists = EmailToken::where(EmailToken::TOKEN, $emailVerifyToken)->exists();

            if (!$exists) {
                $expiresAt = Carbon::now()->addMonths(1);
                EmailToken::query()->create([
                    EmailToken::USER_ID => $user->id(),
                    EmailToken::TOKEN => $emailVerifyToken,
                    EmailToken::EXPIRES_AT => $expiresAt,
                    EmailToken::TENANT_ID => $user->getTenantId()
                ]);
            }
        } while ($exists);


        event(new Registered($user, $emailVerifyToken));

        $token = auth()->login($user);

        return $this->respondWithToken($token, $user->id());
    }

    protected function respondWithToken($token, int $userId)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user_id' => $userId
        ]);
    }
}

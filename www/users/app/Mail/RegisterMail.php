<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $username;
    protected $token;

    /**
     * Create a new message instance.
     * @param string $username
     * @param string $token
     * @return void
     */
    public function __construct(string $username, string $token)
    {
        $this->username = $username;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = url();

        return $this->view('mails.register',
            [
                'username' => $this->username,
                'link' => $link,
                'token' => $this->token
            ]);
    }
}

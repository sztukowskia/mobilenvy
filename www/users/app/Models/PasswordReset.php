<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;

class PasswordReset extends Model
{
    use Notifiable, Tenantable;

    const ID = 'id';
    const TABLE = 'user_password_resets';
    const EMAIL = 'email';
    const TOKEN = 'token';
    const TENANT_ID = 'tenant_id';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::EMAIL, self::TOKEN,
        self::TENANT_ID
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }
}

<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    const TABLE = 'tenants';
    const TENANT_ID = 'tenant_id';
    const ACCESS_KEY = 'access_key';

    protected $table = self::TABLE;
    protected $primaryKey = self::TENANT_ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::TENANT_ID, self::ACCESS_KEY
    ];

    public function id(): int {
        return $this->attributes[self::TENANT_ID];
    }
}

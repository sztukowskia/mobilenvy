<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, Tenantable;

    const TABLE = 'users';
    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const DESCRIPTION = 'description';
    const PASSWORD = 'password';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const IMAGE = 'image';
    const TENANT_ID = 'tenant_id';
    const REMEMBER_TOKEN = 'remember_token';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::NAME, self::EMAIL, self::DESCRIPTION,
        self::PASSWORD, self::EMAIL_VERIFIED_AT, self::IMAGE,
        self::TENANT_ID, self::REMEMBER_TOKEN
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD, self::REMEMBER_TOKEN
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::EMAIL_VERIFIED_AT => 'datetime',
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }

    public function id() {
        return $this->id;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getEmail() {
        return $this->attributes[self::EMAIL];
    }

    public function getTenantId() {
        return $this->attributes[self::TENANT_ID];
    }
}

<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;

class EmailToken extends Model
{
    use Notifiable, Tenantable;

    const TABLE = 'user_email_verification_tokens';
    const ID = 'id';
    const USER_ID = 'user_id';
    const TOKEN = 'token';
    const TENANT_ID = 'tenant_id';
    const EXPIRES_AT = 'expires_at';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::USER_ID, self::TOKEN, self::EXPIRES_AT,
        self::TENANT_ID
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }
}

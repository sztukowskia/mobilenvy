<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class Registered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var App\Models\User
     *
     */
    public $user;

    /**
     * @var string
     */
    public $activationToken;

    /**
     * Registered constructor.
     * @param User $user
     * @param string $activationToken
     */
    public function __construct(User $user, string $activationToken) {
        $this->user = $user;
        $this->activationToken = $activationToken;
    }
}

'use strict';
const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const data = require('./redis.data');

var redis = require('redis');

const subscribe = redis.createClient({
    host: data.exports.host,
    port: data.exports.port
});

subscribe.auth(data.exports.password);
subscribe.psubscribe([`measure.*`, `notification.*`]);

subscribe.on('pmessage', function(pattern, channel, message) {
    console.log("channel: " ,channel);
    console.log("message: ", message);
    console.log(' ');
    message = JSON.parse(message);
    io.emit(channel, message.data);
});

var broadcastPort = 6001;
server.listen(broadcastPort, function () {
    console.log("Socket server is running.");
});

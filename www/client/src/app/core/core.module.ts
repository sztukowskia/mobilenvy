import { NgModule } from '@angular/core';
import { CoreRoutingModule } from './core-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {TranslateModule} from '@ngx-translate/core';
import {CoreComponent} from './core/core.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {CommonModule} from '@angular/common';
import {StationsComponent} from './stations/stations/stations.component';
import {
  NbAlertModule,
  NbButtonModule, NbCardModule, NbCheckboxModule,
  NbIconModule, NbInputModule,
  NbLayoutModule,
  NbMenuModule, NbSelectModule,
  NbSidebarModule,
  NbSidebarService,
  NbTreeGridModule
} from '@nebular/theme';
import {StationsAddComponent} from './stations/add/stations-add.component';
import {StationsEditComponent} from './stations/edit/stations-edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';
import {UnitsComponent} from './units/units/units.component';
import {UnitsAddComponent} from './units/add/units-add.component';
import {UnitsEditComponent} from './units/edit/units-edit.component';
import {WarningsComponent} from './warnings/warnings/warnings.component';
import {WarningsAddComponent} from './warnings/add/warnings-add.component';
import {WarningsEditComponent} from './warnings/edit/warnings-edit.component';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  imports: [
    NbCardModule,
    CommonModule,
    CoreRoutingModule,
    TranslateModule,
    NbLayoutModule,
    NbSidebarModule,
    NbButtonModule,
    NbMenuModule,
    NbIconModule,
    NbTreeGridModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    NbAlertModule,
    FormsModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    ChartsModule
  ],
  declarations: [
    DashboardComponent,
    CoreComponent,
    SidebarComponent,
    StationsComponent,
    StationsAddComponent,
    StationsEditComponent,
    UnitsComponent,
    UnitsAddComponent,
    UnitsEditComponent,
    WarningsComponent,
    WarningsAddComponent,
    WarningsEditComponent
  ],
  providers: [
    NbSidebarService
  ]
})
export class CoreModule {}

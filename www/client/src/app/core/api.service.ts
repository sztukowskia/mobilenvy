import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {catchError, mapTo, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(private http: HttpClient, private router: Router) {}

  public get(endpoint: string, options?: any): Observable<any> {
    return this.http.get<any>(`${environment.servers.server}/${endpoint}`, options);
  }

  public post(endpoint: string, body?: any): Observable<any> {
    return this.http.post<any>(`${environment.servers.server}/${endpoint}`, body);
  }

  public put(endpoint: string, body?: any): Observable<any> {
    return this.http.put<any>(`${environment.servers.server}/${endpoint}`, body);
  }

  public delete(endpoint: string, options?: any): Observable<any> {
    return this.http.delete<any>(`${environment.servers.server}/${endpoint}`, options);
  }
}

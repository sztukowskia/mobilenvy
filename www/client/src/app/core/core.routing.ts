export const CORE_ROUTING = {
  CORE: '',
  STATIONS: 'stations',
  STATIONS_ADD: 'stations/create',
  STATIONS_EDIT: 'stations/edit/:id',
  WARNINGS: 'warnings',
  WARNINGS_ADD: 'warnings/create',
  WARNINGS_EDIT: 'warnings/edit/:id',
  UNITS: 'units',
  UNITS_ADD: 'units/create',
  UNITS_EDIT: 'units/edit/:id'
};

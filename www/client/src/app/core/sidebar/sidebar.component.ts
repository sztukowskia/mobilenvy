import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {NbIconLibraries, NbMenuItem} from '@nebular/theme';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  items: NbMenuItem[] = [
    {
      title: 'Panel główny',
      link: '/app',
      icon: 'home-outline',
    },
    {
      title: 'Stacje badawcze',
      icon: {icon: 'cogs', pack: 'fas'},
      expanded: true,
      children: [
        {
          title: 'Lista',
          link: '/app/stations',
          icon: {icon: 'clipboard-list', pack: 'fas'}
        },
        {
          title: 'Dodaj stację',
          link: '/app/stations/create',
          icon: {icon: 'satellite', pack: 'fas'}
        }
      ]
    },
    {
      title: 'Monitorowane parametry',
      icon: { icon: 'sliders-h', pack: 'fas'},
      expanded: true,
      children: [
        {
          title: 'Lista',
          link: '/app/units',
          icon: {icon: 'clipboard-list', pack: 'fas'}
        },
        {
          title: 'Dodaj parametr',
          link: '/app/units/create',
          icon: {icon: 'star', pack: 'far'}
        }
      ]
    },
    {
      title: 'Powiadomienia',
      icon: {icon: 'envelope-open', pack: 'far'},
      expanded: true,
      children: [
        {
          title: 'Lista',
          link: '/app/warnings',
          icon: {icon: 'clipboard-list', pack: 'fas'}
        },
        {
          title: 'Dodaj powiadomienie',
          link: '/app/warnings/create',
          icon: {icon: 'comment', pack: 'far'}
        }
      ]
    },
    {
      title: 'Wyloguj',
      link: '/app/units',
      icon: 'lock-outline',
    }
  ];

  constructor(private iconLibraries: NbIconLibraries) {
    this.iconLibraries.registerFontPack('fas', {packClass: 'fas', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('far', {packClass: 'far', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('fal', {packClass: 'fal', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('fad', {packClass: 'fad', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('fab', {packClass: 'fab', iconClassPrefix: 'fa'});
  }

  ngOnInit() {

  }

}

import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NbTreeGridPresentationNode} from '@nebular/theme';
import {ApiService} from '../../api.service';
import {NgxSpinnerService} from 'ngx-spinner';

interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  id: number;
  name: string;
  unit: string;
  active: number;
}

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.scss']
})
export class UnitsComponent implements OnInit {

  constructor(private router: Router, private api: ApiService, private spinner: NgxSpinnerService) {}
  actions = {key: 'actions', name: 'Akcje'};
  active =  {key: 'active', name: 'Aktywny'};
  defaultColumns = [
    {key: 'id', name: 'ID'},
    {key: 'name', name: 'Nazwa'},
    {key: 'unit', name: 'Jednostka'},
  ];
  allColumns = [ 'id', 'name', 'unit', 'active', 'actions' ];

  columnNames = ['ID', 'Nazwa', 'Jednostka', 'Aktywny', 'Akcje'];

  data: TreeNode<FSEntry>[] = [];


  ngOnInit(): void {
    this.spinner.show();
    this.api.get('api/stations/unit').subscribe(response => {
      const apiData = [];
      response.units.forEach(station => {
        apiData.push({
          data: station
        });
      });
      this.data = apiData;
      this.spinner.hide();
    });
  }


  public edit(id: number) {
    this.router.navigateByUrl(`app/units/edit/${id}`);
  }

  public create() {
    this.router.navigateByUrl('app/units/create');
  }

  public switchUnit(row: NbTreeGridPresentationNode<FSEntry>) {
    this.spinner.show();
    if (row.data.active) {
      row.data.active = 0;
    } else {
      row.data.active = 1;
    }

    this.api.post('api/stations/unit/activate', {
      unit_id: row.data.id,
      active: row.data.active
    }).subscribe(response => {
      this.spinner.hide();
      console.log(response);
    }, error1 =>  {
      this.spinner.hide();
    });
  }
}

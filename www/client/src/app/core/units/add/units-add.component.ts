import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../api.service';
import Swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {Unit} from '../unit.interface';

@Component({
  selector: 'app-units-add',
  templateUrl: './units-add.component.html',
  styleUrls: ['./units-add.component.scss']
})
export class UnitsAddComponent implements OnInit{

  addUnitForm: FormGroup;
  unit: Unit = {
    name: null,
    unit: null,
    active: null
  };

  constructor(private api: ApiService,
              private spinner: NgxSpinnerService,
              private router: Router
  ) {}

  ngOnInit(): void {
    this.addUnitForm = new FormGroup({
      name: new FormControl(this.unit.name, [
        Validators.required,
      ]),
      unit: new FormControl(this.unit.unit, [
        Validators.required,
      ]),
      active: new FormControl(this.unit.active),
    });
  }

  get name() { return this.addUnitForm.get('name'); }
  get param_unit() { return this.addUnitForm.get('unit'); }

  public submit() {
    this.spinner.show();
    this.api.post('api/stations/unit/create', this.addUnitForm.getRawValue()).subscribe(response => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'success',
        title: '<span style="color: lightgreen;">Utworzono pomyślnie</span>'
      }).then(() => {
        this.router.navigateByUrl('app/units');
      });
    }, error1 => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'error',
        title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
      });
    });
  }
}

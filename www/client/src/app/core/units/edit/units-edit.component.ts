import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../api.service';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';
import {Unit} from '../unit.interface';

@Component({
  selector: 'app-units-edit',
  templateUrl: './units-edit.component.html',
  styleUrls: ['./units-edit.component.scss']
})
export class UnitsEditComponent implements OnInit {


  editUnitForm: FormGroup;
  unit: Unit = {
    name: null,
    unit: null,
    active: null
  };
  id: number = null;

  constructor(private api: ApiService,
              private spinner: NgxSpinnerService,
              private router: Router,
              private activeAouter: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.spinner.show();

    this.id = this.activeAouter.snapshot.params.id;
    this.editUnitForm = new FormGroup({
      name: new FormControl(this.unit.name, [
        Validators.required,
      ]),
      unit: new FormControl(this.unit.unit, [
        Validators.required,
      ]),
      active: new FormControl(this.unit.active),
    });

    this.api.get(`api/stations/unit/${this.id}`).subscribe(response => {
      const unit = response.unit;

      this.editUnitForm.setValue({
        name: unit.name,
        unit: unit.unit,
        active: unit.active
      });
      this.spinner.hide();
    }, error1 => {
      this.spinner.hide();
      this.router.navigateByUrl('app/units');
    });
  }

  get name() { return this.editUnitForm.get('name'); }
  get param_unit() { return this.editUnitForm.get('unit'); }

  public submit() {
    this.spinner.show();
    this.api.put(`api/stations/unit/update/${this.id}`, this.editUnitForm.getRawValue()).subscribe(response => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'success',
        title: '<span style="color: lightgreen;">Zaktualizowano pomyślnie</span>'
      }).then(() => {
        this.router.navigateByUrl('app/units');
      });
    }, error1 => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'error',
        title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
      });
    });
  }


  public delete() {
    Swal.fire({
      title: '<span class="text-warning">Jesteś pewien?</span>',
      text: 'Ta operacja nie będzie mogła być cofnięta',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Tak, usuń!',
      cancelButtonText: 'Nie'
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.api.delete(`api/stations/unit/delete/${this.id}`).subscribe(response => {
          this.spinner.hide();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000
          });
          Toast.fire({
            type: 'success',
            title: '<span style="color: lightgreen;">Pomyślnie usunięto parametr</span>'
          }).then(() => {
            this.router.navigateByUrl('app/units');
          });
        }, error1 => {
          this.spinner.hide();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            type: 'error',
            title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
          });
        });
      }
    });
  }
}

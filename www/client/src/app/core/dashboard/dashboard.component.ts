import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import {ApiService} from '../api.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Socket} from 'ngx-socket-io';

interface Measure {
  station_id: number;
  measure: number;
  unit_id: number;
  created_at: string;
}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private readonly USER_ID = 'USER_ID';

  public data = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis',
          position: 'left',
          gridLines: {
            color: 'rgba(255,255,255,0.3)',
          },
          ticks: {
            fontColor: 'white',
          }
        }
      ]
    },
    annotation: {
    },
  };
  public lineChartColors: Color[] = [
    { // dark grey
      backgroundColor: 'rgba(84, 47, 158, 0.2)',
      borderColor: 'rgba(224, 19, 235, 1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(private api: ApiService, private spinner: NgxSpinnerService, private socket: Socket) { }

  ngOnInit() {
    this.spinner.show();
    this.api.post('api/stations/data').subscribe(response => {
      this.data = response.data;
      console.log(response);
      this.spinner.hide();
    });
    const userId = localStorage.getItem(this.USER_ID);
    this.socket.connect();
    this.socket.on(`measure.${userId}`, (data: Measure) => {
      this.data.forEach(row => {
        if (Number(row.station_id) === Number(data.station_id)) {
          row.units.forEach( unit => {
            if (Number(unit.unit_id) === Number(data.unit_id)) {
              unit.set[0].data.push(data.measure);
              unit.labels.push(data.created_at);

              if (unit.set[0].data.length > 15) {
                unit.set[0].data.shift();
                unit.labels.shift();
              }
            }
          });
        }
      });
    });
  }

  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CORE_ROUTING} from './core.routing';
import {CoreGuard} from './core.guard';
import {CoreComponent} from './core/core.component';
import {StationsComponent} from './stations/stations/stations.component';
import {StationsAddComponent} from './stations/add/stations-add.component';
import {StationsEditComponent} from './stations/edit/stations-edit.component';
import {UnitsComponent} from './units/units/units.component';
import {UnitsAddComponent} from './units/add/units-add.component';
import {UnitsEditComponent} from './units/edit/units-edit.component';
import {WarningsComponent} from './warnings/warnings/warnings.component';
import {WarningsAddComponent} from './warnings/add/warnings-add.component';
import {WarningsEditComponent} from './warnings/edit/warnings-edit.component';

const coreRoutes: Routes = [
  {
    path: CORE_ROUTING.CORE,
    canActivate: [CoreGuard],
    component: CoreComponent,
    children: [
      {path: CORE_ROUTING.CORE, component: DashboardComponent},
      {path: CORE_ROUTING.STATIONS, component: StationsComponent},
      {path: CORE_ROUTING.STATIONS_ADD, component: StationsAddComponent},
      {path: CORE_ROUTING.STATIONS_EDIT, component: StationsEditComponent},
      {path: CORE_ROUTING.UNITS, component: UnitsComponent},
      {path: CORE_ROUTING.UNITS_ADD, component: UnitsAddComponent},
      {path: CORE_ROUTING.UNITS_EDIT, component: UnitsEditComponent},
      {path: CORE_ROUTING.WARNINGS, component: WarningsComponent},
      {path: CORE_ROUTING.WARNINGS_ADD, component: WarningsAddComponent},
      {path: CORE_ROUTING.WARNINGS_EDIT, component: WarningsEditComponent},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(coreRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CoreRoutingModule {}

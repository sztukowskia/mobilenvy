export interface Warning {
  min_value: number;
  max_value: number;
  active: boolean|null;
  unit_id: number;
  unit: {
    id: number;
    name: string;
  };
}

import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NbTreeGridPresentationNode} from '@nebular/theme';
import {ApiService} from '../../api.service';
import {NgxSpinnerService} from 'ngx-spinner';

interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  id: number;
  param: string;
  min_value: number;
  max_value: number;
  active: number;
}

@Component({
  selector: 'app-warnings',
  templateUrl: './warnings.component.html',
  styleUrls: ['./warnings.component.scss']
})
export class WarningsComponent implements OnInit {

  constructor(private router: Router, private api: ApiService, private spinner: NgxSpinnerService) {}
  actions = {key: 'actions', name: 'Akcje'};
  active =  {key: 'active', name: 'Aktywne'};
  defaultColumns = [
    {key: 'id', name: 'ID'},
    {key: 'param', name: 'Parametr'},
    {key: 'min_value', name: 'Dolna dopuszczalna granica'},
    {key: 'max_value', name: 'Górna dopuszczalna granica'},
  ];
  allColumns = [ 'id', 'param', 'min_value', 'max_value', 'active', 'actions' ];

  columnNames = ['ID', 'Parametr', 'Dolna dopuszczalna granica', 'Górna dopuszczalna granica', 'Aktywne', 'Akcje'];

  data: TreeNode<FSEntry>[] = [];


  ngOnInit(): void {
    this.spinner.show();
    this.api.get('api/stations/warning').subscribe(response => {
      const apiData = [];
      response.warnings.forEach(warning => {
        apiData.push({
          data: {
            id: warning.id,
            param: `${warning.unit.name} [${warning.unit.unit}]`,
            min_value: warning.min_value,
            max_value: warning.max_value,
            active: warning.active,
          }
        });
      });
      this.data = apiData;
      this.spinner.hide();
    });
  }


  public edit(id: number) {
    this.router.navigateByUrl(`app/warnings/edit/${id}`);
  }

  public create() {
    this.router.navigateByUrl('app/warnings/create');
  }

  public switchWarning(row: NbTreeGridPresentationNode<FSEntry>) {
    this.spinner.show();
    if (row.data.active) {
      row.data.active = 0;
    } else {
      row.data.active = 1;
    }

    this.api.post('api/stations/warning/activate', {
      warning_id: row.data.id,
      active: row.data.active
    }).subscribe(response => {
      this.spinner.hide();
      console.log(response);
    }, error1 =>  {
      this.spinner.hide();
    });
  }
}

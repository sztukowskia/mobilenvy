import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../api.service';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';
import {Warning} from '../warning.interface';

@Component({
  selector: 'app-warnings-edit',
  templateUrl: './warnings-edit.component.html',
  styleUrls: ['./warnings-edit.component.scss']
})
export class WarningsEditComponent implements OnInit {
  list = [];

  editWarningForm: FormGroup;
  warning: Warning = {
    min_value: null,
    max_value: null,
    active: null,
    unit_id: null,
    unit: {
      id: null,
      name: null,
    }
  };
  id: number = null;

  constructor(private api: ApiService,
              private spinner: NgxSpinnerService,
              private router: Router,
              private activeAouter: ActivatedRoute
  ) {
    this.id = this.activeAouter.snapshot.params.id;
  }

  ngOnInit(): void {
    this.spinner.show();

    this.editWarningForm = new FormGroup({
      unit_id: new FormControl(this.warning.unit_id, [
        Validators.required,
      ]),
      min_value: new FormControl(this.warning.min_value, [
        Validators.required,
      ]),
      max_value: new FormControl(this.warning.max_value, [
        Validators.required,
      ]),
      active: new FormControl(this.warning.active),
    });

    this.api.get(`api/stations/warning/${this.id}`).subscribe(response => {
      this.list = response.list;
      this.warning = {
        min_value: response.warning.min_value,
        max_value: response.warning.max_value,
        active: response.warning.active,
        unit_id: response.warning.unit_id,
        unit: {
          id: null,
          name: null
        }
      };

      this.editWarningForm.setValue({
        unit_id: this.warning.unit_id,
        min_value: this.warning.min_value,
        max_value: this.warning.max_value,
        active: this.warning.active
      });
      this.spinner.hide();

      if (this.list.length === 0) {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 5000
        });
        Toast.fire({
          type: 'error',
          title: '<span style="color: lightcoral;">Nie posiadasz żadnych monitorowanych parametrów!</span>'
        }).then(() => {
          this.router.navigateByUrl('app/warnings');
        });
      }
    });
  }

  get unit_id() { return this.editWarningForm.get('unit_id'); }
  get min_value() { return this.editWarningForm.get('min_value'); }
  get max_value() { return this.editWarningForm.get('max_value'); }

  public submit() {
    this.spinner.show();
    this.api.put(`api/stations/warning/update/${this.id}`, this.editWarningForm.getRawValue()).subscribe(response => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'success',
        title: '<span style="color: lightgreen;">Zaktualizowano pomyślnie</span>'
      }).then(() => {
        this.router.navigateByUrl('app/warnings');
      });
    }, error1 => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'error',
        title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
      });
    });
  }


  public delete() {
    Swal.fire({
      title: '<span class="text-warning">Jesteś pewien?</span>',
      text: 'Ta operacja nie będzie mogła być cofnięta',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Tak, usuń!',
      cancelButtonText: 'Nie'
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.api.delete(`api/stations/warning/delete/${this.id}`).subscribe(response => {
          this.spinner.hide();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000
          });
          Toast.fire({
            type: 'success',
            title: '<span style="color: lightgreen;">Pomyślnie usunięto powiadomienie</span>'
          }).then(() => {
            this.router.navigateByUrl('app/warnings');
          });
        }, error1 => {
          this.spinner.hide();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            type: 'error',
            title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
          });
        });
      }
    });
  }
}

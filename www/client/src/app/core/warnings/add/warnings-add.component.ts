import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../api.service';
import Swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {Warning} from '../warning.interface';
import {empty} from 'rxjs/internal/Observer';

@Component({
  selector: 'app-warnings-add',
  templateUrl: './warnings-add.component.html',
  styleUrls: ['./warnings-add.component.scss']
})
export class WarningsAddComponent implements OnInit{

  list: [];
  addWarningForm: FormGroup;
  warning: Warning = {
    min_value: null,
    max_value: null,
    active: null,
    unit_id: null,
    unit: {
      id: null,
      name: null,
    }
  };

  constructor(private api: ApiService,
              private spinner: NgxSpinnerService,
              private router: Router
  ) {}

  ngOnInit(): void {
    this.spinner.show();
    this.api.post('api/stations/warning/list').subscribe(response => {
      this.list = response.list;
      this.spinner.hide();
      if (this.list.length === 0) {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 5000
        });
        Toast.fire({
          type: 'error',
          title: '<span style="color: lightcoral;">Nie posiadasz żadnych monitorowanych parametrów!</span>'
        }).then(() => {
          this.router.navigateByUrl('app/warnings');
        });
      }
    });


    this.addWarningForm = new FormGroup({
      unit_id: new FormControl(this.warning.unit_id, [
        Validators.required,
      ]),
      min_value: new FormControl(this.warning.min_value, [
        Validators.required,
      ]),
      max_value: new FormControl(this.warning.max_value, [
        Validators.required,
      ]),
      active: new FormControl(this.warning.active),
    });
  }

  get unit_id() { return this.addWarningForm.get('unit_id'); }
  get min_value() { return this.addWarningForm.get('min_value'); }
  get max_value() { return this.addWarningForm.get('max_value'); }

  public submit() {
    this.spinner.show();
    this.api.post('api/stations/warning/create', this.addWarningForm.getRawValue()).subscribe(response => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'success',
        title: '<span style="color: lightgreen;">Utworzono pomyślnie</span>'
      }).then(() => {
        this.router.navigateByUrl('app/warnings');
      });
    }, error1 => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'error',
        title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
      });
    });
  }
}

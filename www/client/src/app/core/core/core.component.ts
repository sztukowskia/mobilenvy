import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {APP_ROUTING} from '../../app.routing';
import {Socket} from 'ngx-socket-io';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent implements OnInit {
  private readonly USER_ID = 'USER_ID';
  constructor(private authService: AuthService, private router: Router, private socket: Socket) { }

  ngOnInit() {
    this.socket.connect();
    const userId = localStorage.getItem(this.USER_ID);

    this.socket.on(`notification.${userId}`, (data) => {
      console.log(data);
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showCloseButton: true,
        showConfirmButton: false
      });

      Toast.fire({
        type: 'warning',
        title: `<span class="text-warning">Uwaga! Stacja nr: ${data.station_id} zarejestrowała niewłaściwy pomiar. Parametr: ${data.unit}, wartość: ${data.measure}, dopuszczalny zakres: ${data.min_value} - ${data.max_value} </span>`
      });
    });
  }

  logout() {
    this.authService.logout().subscribe(res => {
      this.router.navigateByUrl(APP_ROUTING.HOME);
    }, error1 => {
      console.log(error1);
    });
  }
}

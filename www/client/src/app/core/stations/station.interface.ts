export interface Station {
  latitude: number;
  longitude: number;
  station_key: string;
  active: boolean|null;
}

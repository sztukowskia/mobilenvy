import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationsAddComponent } from './stations-add.component';

describe('CoreComponent', () => {
  let component: StationsAddComponent;
  let fixture: ComponentFixture<StationsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

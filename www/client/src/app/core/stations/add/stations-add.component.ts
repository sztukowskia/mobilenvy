import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../api.service';
import Swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {Station} from '../station.interface';

@Component({
  selector: 'app-stations-add',
  templateUrl: './stations-add.component.html',
  styleUrls: ['./stations-add.component.scss']
})
export class StationsAddComponent implements OnInit{

  addStationForm: FormGroup;
  station: Station = {
    latitude: null,
    longitude: null,
    station_key: null,
    active: null
  };

  constructor(private api: ApiService,
              private spinner: NgxSpinnerService,
              private router: Router
  ) {}

  ngOnInit(): void {
    this.addStationForm = new FormGroup({
      latitude: new FormControl(this.station.latitude, [
        Validators.required,
        Validators.min(0),
        Validators.max(90)
      ]),
      longitude: new FormControl(this.station.longitude, [
        Validators.required,
        Validators.min(0),
        Validators.max(180)
      ]),
      station_key: new FormControl(this.station.station_key, [
        Validators.required,
      ]),
      active: new FormControl(this.station.active),
    });
  }

  get latitude() { return this.addStationForm.get('latitude'); }
  get longitude() { return this.addStationForm.get('longitude'); }
  get station_key() { return this.addStationForm.get('station_key'); }

  public submit() {
    this.spinner.show();
    this.api.post('api/stations/station/create', this.addStationForm.getRawValue()).subscribe(response => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'success',
        title: '<span style="color: lightgreen;">Utworzono pomyślnie</span>'
      }).then(() => {
        this.router.navigateByUrl('app/stations');
      });
    }, error1 => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'error',
        title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
      });
    });
  }
}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../api.service';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';
import {Station} from '../station.interface';

@Component({
  selector: 'app-stations-edit',
  templateUrl: './stations-edit.component.html',
  styleUrls: ['./stations-edit.component.scss']
})
export class StationsEditComponent implements OnInit {


  editStationForm: FormGroup;
  station: Station = {
    latitude: null,
    longitude: null,
    station_key: null,
    active: null
  };
  id: number = null;

  constructor(private api: ApiService,
              private spinner: NgxSpinnerService,
              private router: Router,
              private activeAouter: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.spinner.show();

    this.id = this.activeAouter.snapshot.params.id;
    this.editStationForm = new FormGroup({
      latitude: new FormControl(this.station.latitude, [
        Validators.required,
        Validators.min(0),
        Validators.max(90)
      ]),
      longitude: new FormControl(this.station.longitude, [
        Validators.required,
        Validators.min(0),
        Validators.max(180)
      ]),
      station_key: new FormControl(this.station.station_key, [
        Validators.required,
      ]),
      active: new FormControl(this.station.active),
    });

    this.api.get(`api/stations/station/${this.id}`).subscribe(response => {
      const station = response.station;

      this.editStationForm.setValue({
        latitude: station.latitude,
        longitude: station.longitude,
        station_key: station.access_key,
        active: station.active
      });
      this.spinner.hide();
    }, error1 => {
      this.spinner.hide();
      this.router.navigateByUrl('app/stations');
    });
  }

  get latitude() { return this.editStationForm.get('latitude'); }
  get longitude() { return this.editStationForm.get('longitude'); }
  get station_key() { return this.editStationForm.get('station_key'); }

  public submit() {
    this.spinner.show();
    this.api.put(`api/stations/station/update/${this.id}`, this.editStationForm.getRawValue()).subscribe(response => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'success',
        title: '<span style="color: lightgreen;">Zaktualizowano pomyślnie</span>'
      }).then(() => {
        this.router.navigateByUrl('app/stations');
      });
    }, error1 => {
      this.spinner.hide();
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: 'error',
        title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
      });
    });
  }


  public delete() {
    Swal.fire({
      title: '<span class="text-warning">Jesteś pewien?</span>',
      text: 'Ta operacja nie będzie mogła być cofnięta',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Tak, usuń!',
      cancelButtonText: 'Nie'
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.api.delete(`api/stations/station/delete/${this.id}`).subscribe(response => {
          this.spinner.hide();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000
          });
          Toast.fire({
            type: 'success',
            title: '<span style="color: lightgreen;">Pomyślnie usunięto stację</span>'
          }).then(() => {
            this.router.navigateByUrl('app/stations');
          });
        }, error1 => {
          this.spinner.hide();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            type: 'error',
            title: '<span style="color: lightcoral;">Wystąpił błąd! Spróbuj ponownie później</span>'
          });
        });
      }
    });
  }
}

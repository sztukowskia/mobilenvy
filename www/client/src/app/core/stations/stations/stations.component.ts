import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NbTreeGridPresentationNode} from '@nebular/theme';
import {ApiService} from '../../api.service';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';

interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  id: number;
  latitude: number;
  longitude: number;
  active: number;
}

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.scss']
})
export class StationsComponent implements OnInit {

  constructor(private router: Router, private api: ApiService, private spinner: NgxSpinnerService) {}
  actions = {key: 'actions', name: 'Akcje'};
  active =  {key: 'active', name: 'Aktywna'};
  defaultColumns = [
    {key: 'id', name: 'ID'},
    {key: 'latitude', name: 'Szerokość geograficzna'},
    {key: 'longitude', name: 'Długość geograficzna'},
  ];
  allColumns = [ 'id', 'latitude', 'longitude', 'active', 'actions' ];

  columnNames = ['ID', 'Szerokość geograficzna', 'Szerokość geograficzna', 'Aktywna', 'Akcje'];

  data: TreeNode<FSEntry>[] = [];


  ngOnInit(): void {
    this.spinner.show();
    this.api.get('api/stations/station').subscribe(response => {
      const apiData = [];
      response.stations.forEach(station => {
        apiData.push({
          data: station
        });
      });
      this.data = apiData;
      this.spinner.hide();
    });
  }


  public edit(id: number) {
    this.router.navigateByUrl(`app/stations/edit/${id}`);
  }

  public create() {
    this.router.navigateByUrl('app/stations/create');
  }

  public switchStation(row: NbTreeGridPresentationNode<FSEntry>) {
    this.spinner.show();
    if (row.data.active) {
      row.data.active = 0;
    } else {
      row.data.active = 1;
    }

    this.api.post('api/stations/station/activate', {
      station_id: row.data.id,
      active: row.data.active
    }).subscribe(response => {
      this.spinner.hide();
      console.log(response);
    }, error1 =>  {
      this.spinner.hide();
    });
  }
}

export const APP_ROUTING = {
  HOME: '',
  APP: 'app',
  PAGE_NOT_FOUND: '**',
  AUTH: 'auth'
};

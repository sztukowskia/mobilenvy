<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('access.token')->group(function () {
    /**
     * Station endpoints
     */
    Route::prefix('station')->group(function () {
        Route::get('/', 'StationController@getStations');
        Route::get('/{id}', 'StationController@getSingleStation');
        Route::post('create', 'StationController@createStation');
        Route::put('update/{id}', 'StationController@updateStation');
        Route::post('activate', 'StationController@activate');
        Route::delete('delete/{id}', 'StationController@delete');
    });

    /**
     * Unit endpoints
     */
    Route::prefix('unit')->group(function () {
        Route::get('/', 'UnitController@getUnits');
        Route::get('/{id}', 'UnitController@getSingleUnit');
        Route::post('create', 'UnitController@createUnit');
        Route::put('update/{id}', 'UnitController@updateUnit');
        Route::post('activate', 'UnitController@activate');
        Route::delete('delete/{id}', 'UnitController@delete');
    });

    /**
     * Warnings endpoints
     */
    Route::prefix('warning')->group(function () {
        Route::get('/', 'WarningController@getWarnings');
        Route::get('/{id}', 'WarningController@getSingleWarning');
        Route::post('list', 'WarningController@getUnitList');
        Route::post('create', 'WarningController@createWarning');
        Route::put('update/{id}', 'WarningController@updateWarning');
        Route::post('activate', 'WarningController@activate');
        Route::delete('delete/{id}', 'WarningController@delete');
    });

    Route::post('data', 'StationController@getData');
});

/**
 * Measure endpoints
 */
Route::prefix('measure')->group(function () {
    Route::post('create', 'MeasureController@createMeasure');
});

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('test', function () {
    event(new \App\Events\MeasureEvent(1, [
        'unit_id' => 3,
        'measure' => 10,
        'measure_time' => '2019-10-23 16:35:10'
    ]));

    return 'fired';
});
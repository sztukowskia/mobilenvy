<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    const TABLE = 'station_notifications';
    const TENANTS_TABLE = 'tenants';
    const MEASURES_TABLE = 'station_measures';
    const WARNINGS_TABLE = 'station_warnings';
    const TENANT_ID = 'tenant_id';
    const MEASURE_ID = 'measure_id';
    const WARNING_ID = 'warning_id';
    const ID = 'id';
    const ACTIVE = 'active';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->integer(self::TENANT_ID, false, true);
                $table->increments(self::ID);
                $table->integer(self::MEASURE_ID, false, true);
                $table->integer(self::WARNING_ID, false, true);
                $table->boolean(self::ACTIVE);
                $table->timestamps();

                $table->foreign(self::TENANT_ID)->references(self::TENANT_ID)->on(self::TENANTS_TABLE);
                $table->foreign(self::WARNING_ID)->references(self::ID)->on(self::WARNINGS_TABLE);
                $table->foreign(self::MEASURE_ID)->references(self::ID)->on(self::MEASURES_TABLE);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::TABLE);
        Schema::enableForeignKeyConstraints();
    }
}

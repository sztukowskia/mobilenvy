<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarningsTable extends Migration
{
    const TABLE = 'station_warnings';
    const TENANTS_TABLE = 'tenants';
    const UNITS_TABLE = 'station_units';
    const TENANT_ID = 'tenant_id';
    const UNIT_ID = 'unit_id';
    const ID = 'id';
    const MIN_VALUE = 'min_value';
    const MAX_VALUE = 'max_value';
    const ACTIVE = 'active';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->integer(self::TENANT_ID, false, true);
                $table->increments(self::ID);
                $table->integer(self::UNIT_ID, false, true);
                $table->float(self::MIN_VALUE);
                $table->float(self::MAX_VALUE);
                $table->boolean(self::ACTIVE);
                $table->timestamps();

                $table->foreign(self::TENANT_ID)->references(self::TENANT_ID)->on(self::TENANTS_TABLE);
                $table->foreign(self::UNIT_ID)->references(self::ID)->on(self::UNITS_TABLE);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::TABLE);
        Schema::enableForeignKeyConstraints();
    }
}

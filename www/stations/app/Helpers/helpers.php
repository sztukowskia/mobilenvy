<?php
namespace App\Helpers;

use App\Models\Tenant;
use Illuminate\Support\Str;

if (!function_exists('tenant')) {
    function tenant(): Tenant {
        return Tenant::where(Tenant::ACCESS_KEY, request()->get('ACCESS_KEY'))->first();
    }
};

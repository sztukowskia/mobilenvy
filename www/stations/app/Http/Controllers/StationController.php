<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActivateStationRequest;
use App\Http\Requests\DataRequest;
use App\Http\Requests\StationRequest;
use App\Services\StationService;
use Illuminate\Http\Request;

class StationController extends Controller
{

    /**
     * @var StationService
     */
    private $stationService;

    /**
     * StationController constructor.
     * @param StationService $stationService
     */
    public function __construct(StationService $stationService)
    {
        $this->stationService = $stationService;
    }

    public function getStations(Request $request) {
        $stations = $this->stationService->getAllStations($request);
        return $this->response(true, 200, ['stations' => $stations]);
    }

    public function getSingleStation(Request $request, $id) {
        $station = $this->stationService->getStation($request, $id);

        if (empty($station)) {
            return $this->response(true, 403);
        }
        return $this->response(true, 200, ['station' => $station]);

    }

    public function delete(Request $request, int $id) {
        if ($this->stationService->delete($request, $id)) {
            return $this->response(true, 200);
        }
        return $this->response(true, 403);
    }

    public function activate(ActivateStationRequest $request) {
        $validated = $request->validated();

        if ($this->stationService->activate($request)) {
            return $this->response(true, 200);
        }
        return $this->response(false, 403);
    }

    public function createStation(StationRequest $stationRequest) {
        $validated = $stationRequest->validated();
        if ($this->stationService->createStation($stationRequest)) {
            return $this->response(true, 201);
        }
        return $this->response(false, 403);
    }

    public function updateStation(StationRequest $stationRequest, int $id) {
        $validated = $stationRequest->validated();
        if ($this->stationService->updateStation($stationRequest, $id)) {
            return $this->response(true, 201);
        }
        return $this->response(false, 403);
    }

    public function getData(DataRequest $request) {
        $data = $this->stationService->getData($request);

        if ($data) {
            return $this->response(true, 200, ['data' => $data]);
        }
        return $this->response(false, 403);
    }
}

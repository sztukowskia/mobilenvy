<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeasureRequest;
use App\Services\MeasureService;
use App\Services\StationService;

class MeasureController extends Controller
{

    /**
     * @var MeasureService
     */
    private $measureService;

    /**
     * @var StationService
     */
    private $stationService;

    /**
     * StationController constructor.
     * @param StationService $stationService
     * @param MeasureService $measureService
     */
    public function __construct(StationService $stationService, MeasureService $measureService)
    {
        $this->stationService = $stationService;
        $this->measureService = $measureService;
    }

    public function createMeasure(MeasureRequest $measureRequest) {
        $validated = $measureRequest->validated();
        try {
            if ($this->measureService->createMeasure($measureRequest)) {
                return $this->response(true, 201);
            } else {
                return $this->response(false, 403);
            }
        } catch (\Exception $e) {
            return $this->response(false, 403);
        }

    }
}

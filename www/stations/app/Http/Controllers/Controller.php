<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response(bool $success, int $status, array $message = []) {
        $response = ['success' => $success];

        if (!empty($message)) {
            $response = array_merge($response, $message);
        }
        return response($response, $status);
    }
}

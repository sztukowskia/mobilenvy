<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActivateWarningRequest;
use App\Http\Requests\WarningRequest;
use App\Services\WarningService;
use Illuminate\Http\Request;

class WarningController extends Controller
{

    /**
     * @var WarningService $warningService
     */
    private $warningService;

    /**
     * StationController constructor.
     * @param WarningService $warningService
     */
    public function __construct(WarningService $warningService)
    {
        $this->warningService = $warningService;
    }

    public function getWarnings(Request $request) {
        $warnings = $this->warningService->getAllWarnings($request);
        return $this->response(true, 200, ['warnings' => $warnings]);
    }

    public function getSingleWarning(Request $request, $id) {
        $warning = $this->warningService->getWarning($request, $id);
        $list = $this->warningService->list($request);

        if (empty($warning) || empty($list)) {
            return $this->response(true, 403);
        }
        return $this->response(true, 200, [
            'warning' => $warning,
            'list' => $list
        ]);
    }

    public function delete(Request $request, int $id) {
        if ($this->warningService->delete($request, $id)) {
            return $this->response(true, 200);
        }
        return $this->response(true, 403);
    }

    public function activate(ActivateWarningRequest $request) {
        $validated = $request->validated();

        if ($this->warningService->activate($request)) {
            return $this->response(true, 200);
        }
        return $this->response(false, 403);
    }

    public function createWarning(WarningRequest $warningRequest) {
        $validated = $warningRequest->validated();
        if ($this->warningService->createWarning($warningRequest)) {
            return $this->response(true, 200);
        } else {
            return $this->response(false, 403);
        }
    }

    public function updateWarning(WarningRequest $warningRequest, int $id) {
        $validated = $warningRequest->validated();
        if ($this->warningService->updateWarning($warningRequest, $id)) {
            return $this->response(true, 200);
        } else {
            return $this->response(false, 403);
        }
    }

    public function getUnitList(Request $request) {
        try {

            $list = $this->warningService->list($request);
            return $this->response(true, 200, [
                'list' => $list
            ]);

        } catch (\Exception $e) {
            return $this->response(false, 500, ['err' => $e->getMessage()]);
        }

    }
}

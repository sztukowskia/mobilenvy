<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActivateUnitRequest;
use App\Http\Requests\UnitRequest;
use App\Services\UnitService;
use Illuminate\Http\Request;

class UnitController extends Controller
{

    /**
     * @var UnitService
     */
    private $unitService;

    /**
     * UnitController constructor.
     * @param UnitService $unitService
     */
    public function __construct(UnitService $unitService)
    {
        $this->unitService = $unitService;
    }

    public function getUnits(Request $request) {
        $units = $this->unitService->getAllUnits($request);
        return $this->response(true, 200, ['units' => $units]);
    }

    public function getSingleUnit(Request $request, $id) {
        $unit = $this->unitService->getUnit($request, $id);

        if (empty($unit)) {
            return $this->response(true, 403);
        }
        return $this->response(true, 200, ['unit' => $unit]);

    }

    public function delete(Request $request, int $id) {
        if ($this->unitService->delete($request, $id)) {
            return $this->response(true, 200);
        }
        return $this->response(true, 403);
    }

    public function activate(ActivateUnitRequest $request) {
        $validated = $request->validated();

        if ($this->unitService->activate($request)) {
            return $this->response(true, 200);
        }
        return $this->response(false, 403);
    }

    //---------------------------------------------------------------

    public function createUnit(UnitRequest $unitRequest) {
        $validated = $unitRequest->validated();
        if ($this->unitService->createUnit($unitRequest)) {
            return $this->response(true, 201);
        } else {
            return $this->response(false, 403);
        }
    }

    public function updateUnit(UnitRequest $unitRequest, int $id) {
        $validated = $unitRequest->validated();
        if ($this->unitService->updateUnit($unitRequest, $id)) {
            return $this->response(true, 200);
        } else {
            return $this->response(false, 403);
        }
    }
}

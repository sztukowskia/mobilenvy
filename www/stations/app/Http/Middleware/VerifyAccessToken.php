<?php

namespace App\Http\Middleware;

use App\Models\Tenant;
use Closure;

class VerifyAccessToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accessToken = $request->get('ACCESS_KEY');
        Tenant::where(Tenant::ACCESS_KEY, $accessToken)->firstOrFail();

        return $next($request);
    }
}

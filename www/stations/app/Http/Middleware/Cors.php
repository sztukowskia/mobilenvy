<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        header('Access-Control-Allow-Origin: '.implode(', ', config('cors.allowedOrigins')));
        header('Access-Control-Allow-Methods: '.implode(', ', config('cors.allowedMethods')));
        header('Access-Control-Allow-Credentials: true');
        header("Access-Control-Allow-Headers: ".implode(', ', config('cors.allowedHeaders')));
        return $next($request);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeasureRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'station_key' => 'required|string',
            'unit_id' => 'required|numeric',
            'measure' => 'required|numeric',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class WarningRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'min_value' => 'required|numeric',
            'max_value' => 'required|numeric',
            'user' => 'required|numeric',
            'unit_id' => 'required|numeric'
        ];
    }
}

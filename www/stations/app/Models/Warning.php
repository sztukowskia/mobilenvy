<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;

class Warning extends Model
{
    use Notifiable, Tenantable;

    const TABLE = 'station_warnings';
    const TENANTS_TABLE = 'tenants';
    const TENANT_ID = 'tenant_id';
    const ID = 'id';
    const UNIT_ID = 'unit_id';
    const MIN_VALUE = 'min_value';
    const MAX_VALUE = 'max_value';
    const ACTIVE = 'active';

    const WITH_UNIT = 'unit';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::TENANT_ID, self::UNIT_ID,
        self::MIN_VALUE, self::MAX_VALUE, self::ACTIVE
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }

    public function unit(): BelongsTo {
        return $this->belongsTo(Unit::class, self::UNIT_ID, Unit::ID);
    }
}

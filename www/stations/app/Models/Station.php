<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;

class Station extends Model
{
    use Tenantable;

    const TABLE = 'stations';
    const TENANTS_TABLE = 'tenants';
    const TENANT_ID = 'tenant_id';
    const ID = 'id';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const ACTIVE = 'active';
    const ACCESS_KEY = 'access_key';
    const USER_ID = 'user_id';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::LATITUDE, self::LONGITUDE, self::ACCESS_KEY,
        self::ACTIVE, self::TENANT_ID, self::USER_ID
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }

    public function measures(): HasMany {
        return $this->hasMany(Measure::class, Measure::STATION_ID, self::ID);
    }
}

<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;

class Notification extends Model
{
    use Notifiable, Tenantable;

    const TABLE = 'station_notifications';
    const TENANTS_TABLE = 'tenants';
    const TENANT_ID = 'tenant_id';
    const MEASURE_ID = 'measure_id';
    const WARNING_ID = 'warning_id';
    const ID = 'id';
    const ACTIVE = 'active';


    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::TENANT_ID, self::MEASURE_ID,
        self::WARNING_ID, self::ACTIVE
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }

    public function warning(): BelongsTo {
        return $this->belongsTo(Warning::class, self::WARNING_ID, Warning::ID);
    }

    public function measure(): BelongsTo {
        return $this->belongsTo(Measure::class, self::MEASURE_ID, Measure::ID);
    }
}

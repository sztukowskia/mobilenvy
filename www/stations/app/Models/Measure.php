<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;

class Measure extends Model
{
    use Notifiable, Tenantable;

    const TABLE = 'station_measures';
    const TENANTS_TABLE = 'tenants';
    const STATIONS_TABLE = 'stations';
    const UNITS_TABLE = 'station_units';
    const TENANT_ID = 'tenant_id';
    const STATION_ID = 'station_id';
    const UNIT_ID = 'unit_id';
    const ID = 'id';
    const MEASURE = 'measure';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::TENANT_ID, self::STATION_ID,
        self::UNIT_ID, self::MEASURE
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }

    public function station(): BelongsTo {
        return $this->belongsTo(Station::class, self::STATION_ID, Station::ID);
    }

    public function unit(): BelongsTo {
        return $this->belongsTo(Unit::class, self::UNIT_ID, Unit::ID);
    }
}

<?php

namespace App\Models;

use App\Traits\Tenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;

class Unit extends Model
{
    use Notifiable, Tenantable;

    const TABLE = 'station_units';
    const TENANTS_TABLE = 'tenants';
    const TENANT_ID = 'tenant_id';
    const USER_ID = 'user_id';
    const ACTIVE = 'active';
    const ID = 'id';
    const UNIT = 'unit';
    const NAME = 'name';

    const WITH_MEASURES = 'measures';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::TENANT_ID, self::UNIT, self::NAME,
        self::USER_ID, self::ACTIVE
    ];

    public function tenant(): BelongsTo {
        return $this->belongsTo(Tenant::class, self::TENANT_ID, Tenant::TENANT_ID);
    }

    public function measures(): HasMany {
        return $this->hasMany(Measure::class, Measure::UNIT_ID, self::ID);
    }
}

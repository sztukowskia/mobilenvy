<?php

namespace App\Services;

use App\Http\Requests\ActivateWarningRequest;
use App\Http\Requests\WarningRequest;
use App\Models\Measure;
use App\Models\Station;
use App\Models\Unit;
use App\Models\Warning;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class WarningService {

    /**
     * @var Warning $warning
     */
    private $warning;

    /**
     * @var Unit $unit
     */
    private $unit;

    public function __construct(Warning $warning, Unit $unit)
    {
        $this->warning = $warning;
        $this->unit = $unit;
    }

    public function getAllWarnings(Request $request) {
        $userId = $request->get('user');

        $warnings = $this->warning
            ->with($this->warning::WITH_UNIT.':'.$this->unit::ID.','.$this->unit::NAME.','.$this->unit::UNIT)
            ->whereHas($this->warning::WITH_UNIT, function (Builder $query) use ($userId) {
                $query
                    ->where($this->unit::USER_ID, $userId);
            })
            ->get();

        return $warnings;
    }

    public function getWarning(Request $request, int $id) {
        $userId = $request->get('user');

         return $this->warning
            ->whereHas($this->warning::WITH_UNIT, function (Builder $query) use ($userId) {
                $query
                    ->where($this->unit::USER_ID, $userId);
            })
            ->where($this->warning::ID, $id)
            ->firstOrFail();
    }

    public function activate(ActivateWarningRequest $request) {
        $userId = $request->get('user');

        $warning = $this->warning
            ->whereHas($this->warning::WITH_UNIT, function (Builder $query) use ($userId) {
                $query
                    ->where($this->unit::USER_ID, $userId);
            })
            ->where($this->warning::ID, $request->get('warning_id'))
            ->firstOrFail();

        if (!empty($warning)) {
            $warning->active = $request->get('active');
            $warning->save();
            return true;
        }
        return false;
    }

    public function delete(Request $request, int $id) {
        try {
            $userId = $request->get('user');

            $warning = $this->warning
                ->whereHas($this->warning::WITH_UNIT, function (Builder $query) use ($userId) {
                    $query
                        ->where($this->unit::USER_ID, $userId);
                })
                ->where($this->warning::ID, $id)
                ->firstOrFail();


            $warning->delete();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function createWarning(WarningRequest $warningRequest) {
        $active = $warningRequest->get('active');
        $active = $active ? $active : 0;

        $unit = $this->unit
            ->where($this->unit::ID, $warningRequest->get('unit_id'))
            ->where($this->unit::USER_ID, $warningRequest->get('user'))
            ->first();

        if (!empty($unit)) {
            return $this->warning
                ->query()
                ->create([
                    Warning::MIN_VALUE => $warningRequest->get('min_value'),
                    Warning::MAX_VALUE => $warningRequest->get('max_value'),
                    Warning::UNIT_ID => $warningRequest->get('unit_id'),
                    Warning::ACTIVE => $active
                ]);
        }
        return null;
    }

    public function updateWarning(WarningRequest $warningRequest, int $id) {
        $userId = $warningRequest->get('user');

        $active = $warningRequest->get('active');
        $active = $active ? $active : 0;

        $this->warning = $this->warning
            ->whereHas($this->warning::WITH_UNIT, function (Builder $query) use ($userId) {
                $query
                    ->where($this->unit::USER_ID, $userId);
            })
            ->where($this->warning::ID, $id)
            ->firstOrFail();

        return $this->warning->update([
            Warning::MIN_VALUE => $warningRequest->get('min_value'),
            Warning::MAX_VALUE => $warningRequest->get('max_value'),
            Warning::UNIT_ID => $warningRequest->get('unit_id'),
            Warning::ACTIVE => $active
        ]);
    }

    public function list(Request $request) {
        $userId = $request->get('user');

        $params = $this->unit
            ->where($this->unit::USER_ID, $userId)
            ->get();

        $params->makeHidden([
            $this->unit::TENANT_ID,
            $this->unit::CREATED_AT,
            $this->unit::UPDATED_AT,
            $this->unit::USER_ID
        ]);

        return $params;
    }
}

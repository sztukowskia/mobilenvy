<?php

namespace App\Services;

use App\Events\MeasureEvent;
use App\Events\NotificationEvent;
use App\Http\Requests\MeasureRequest;
use App\Models\Measure;
use App\Models\Notification;
use App\Models\Station;
use App\Models\Unit;
use App\Models\Warning;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class MeasureService {

    /**
     * @var Station $station
     */
    private $station;

    /**
     * @var Unit $unit
     */
    private $unit;

    /**
     * @var Measure $measure
     */
    private $measure;

    /**
     * @var Notification $notification
     */
    private $notification;

    /**
     * @var Warning $warning
     */
    private $warning;

    public function __construct(Station $station, Unit $unit, Measure $measure, Notification $notification, Warning $warning)
    {
        $this->station = $station;
        $this->measure = $measure;
        $this->unit = $unit;
        $this->notification = $notification;
        $this->warning = $warning;
    }

    public function createMeasure(MeasureRequest $measureRequest) {
        $station = $this->station
            ->withoutGlobalScopes()
            ->where($this->station::ACCESS_KEY, $measureRequest->post('station_key'))
            ->where($this->station::ACTIVE, 1)
            ->firstOrFail();

        $userId = $station->{$this->station::USER_ID};

        $data = [
            'unit_id' => $measureRequest->post('unit_id'),
            'measure' => $measureRequest->post('measure'),
            'station_id' => $station->{$this->station::ID},
            'created_at' => Carbon::now()->format('Y-m-d H:i'),
        ];

        $this->unit::unsetEventDispatcher();
        $unit = $this->unit
            ->withoutGlobalScopes()
            ->where($this->unit::TENANT_ID, $station->{$this->station::TENANT_ID})
            ->where($this->unit::ID, $data['unit_id'])
            ->where($this->unit::ACTIVE, 1)
            ->where($this->unit::USER_ID, $userId)
            ->firstOrFail();



        $this->measure::unsetEventDispatcher();
        $measure = $this->measure
            ->query()
            ->create([
                Measure::TENANT_ID => $station->{$this->station::TENANT_ID},
                Measure::STATION_ID => $station->{$this->station::ID},
                Measure::UNIT_ID => $data['unit_id'],
                Measure::MEASURE => $data['measure'],
            ]);
        if ($measure) {
            event(new MeasureEvent($station->{$this->station::USER_ID}, $data));
        }


        $this->warning::unsetEventDispatcher();
        $this->notification::unsetEventDispatcher();

        $warning = $this->warning
            ->withoutGlobalScopes()
            ->where($this->warning::ACTIVE, 1)
            ->where($this->warning::TENANT_ID, $station->{$this->station::TENANT_ID})
            ->where($this->warning::UNIT_ID, $data['unit_id'])
            ->where(function (Builder $builder) use ($measure) {
                $builder->orWhere($this->warning::MIN_VALUE, '>', $measure->{$this->measure::MEASURE})
                    ->orWhere($this->warning::MAX_VALUE, '<', $measure->{$this->measure::MEASURE});
            })
            ->first();

        if (!empty($warning)) {
            $notification = $this->notification
                ->query()
                ->create([
                    $this->notification::TENANT_ID => $station->{$this->station::TENANT_ID},
                    $this->notification::MEASURE_ID => $measure->{$this->measure::ID},
                    $this->notification::WARNING_ID => $warning->{$this->warning::ID},
                    $this->notification::ACTIVE => 1,
                ]);

            if ($notification) {
                $data = array_merge($data, [
                    'min_value' => $warning->{$this->warning::MIN_VALUE},
                    'max_value' => $warning->{$this->warning::MAX_VALUE},
                    'unit' => $unit->{$this->unit::NAME}.' ('.$unit->{$this->unit::UNIT}.')',
                    'station_id' => $station->{$this->station::ID}
                ]);
                event(new NotificationEvent($station->{$this->station::USER_ID}, $data));
            }
        }

        return true;
    }

}

<?php

namespace App\Services;

use App\Http\Requests\ActivateStationRequest;
use App\Http\Requests\DataRequest;
use App\Http\Requests\GetStationRequest;
use App\Http\Requests\StationRequest;
use App\Models\Measure;
use App\Models\Station;
use App\Models\Unit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Session\Middleware\StartSession;

class StationService {

    /**
     * @var Station
     */
    private $station;

    /**
     * @var Unit
     */
    private $unit;

    /**
     * @var Measure
     */
    private $measure;

    public function __construct(Station $station, Unit $unit, Measure $measure)
    {
        $this->station = $station;
        $this->measure = $measure;
        $this->unit = $unit;
    }

    public function getAllStations(Request $request) {
        $stations =  $this->station->where(Station::USER_ID, $request->get('user'))
            ->get();

        if (!empty($stations)) {
            $stations->makeHidden([
                Station::ACCESS_KEY,
                Station::TENANT_ID,
                Station::USER_ID,
                Station::CREATED_AT,
                Station::UPDATED_AT
            ]);
        }

        return $stations;
    }

    public function getStation(Request $request, int $id) {
        $station = $this->station
            ->where(Station::USER_ID, $request->get('user'))
            ->where(Station::ID, $id)
            ->first();

        if (!empty($station)) {
            $station->makeHidden([
                Station::CREATED_AT,
                Station::UPDATED_AT,
                Station::USER_ID,
                Station::TENANT_ID
            ]);
        }

        return $station;
    }

    public function activate(ActivateStationRequest $request) {
        $station = $this->station
            ->where(Station::USER_ID, $request->get('user'))
            ->where(Station::ID, $request->get('station_id'))
            ->first();

        if (!empty($station)) {
            $station->active = $request->get('active');
            $station->save();
            return true;
        }
        return false;
    }

    public function delete(Request $request, int $id) {
        try {
            $station = $this->station->where(Station::ID, $id)
                ->where(Station::USER_ID, $request->get('user'))
                ->firstOrFail();

            $this->measure->where(Measure::STATION_ID, $id)->delete();

            $station->delete();
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function createStation(StationRequest $stationRequest) {
        $active = $stationRequest->get('active');
        $active = $active ? $active : 0;

        return $this->station
            ->query()
            ->create([
                Station::LATITUDE => $stationRequest->get('latitude'),
                Station::LONGITUDE => $stationRequest->get('longitude'),
                Station::ACCESS_KEY => $stationRequest->get('station_key'),
                Station::USER_ID => $stationRequest->get('user'),
                Station::ACTIVE => $active
            ]);
    }

    public function updateStation(StationRequest $stationRequest, int $id) {
        $userId = $stationRequest->get('user');
        $this->station = $this->station->where(Station::ID, $id)->where(Station::USER_ID, $userId)->firstOrFail();

        $active = $stationRequest->get('active');
        $active = $active ? $active : 0;

        return $this->station->update([
            Station::LATITUDE => $stationRequest->get('latitude'),
            Station::LONGITUDE => $stationRequest->get('longitude'),
            Station::ACCESS_KEY => $stationRequest->get('station_key'),
            Station::ACTIVE => $active
        ]);
    }

    public function getData(DataRequest $request) {
        $userId = $request->get('user');

        $data = [];
        $stations = $this->station
            ->where($this->station::USER_ID, $userId)
            ->where($this->station::ACTIVE, 1)
            ->get();

        foreach ($stations as $station) {
            $stationRow = [
                'station_id' => $station->{$this->station::ID},
                'units' => []
            ];
            $units = $this->unit
                ->where($this->unit::USER_ID, $userId)
                ->where($this->unit::ACTIVE, 1)
                ->with($this->unit::WITH_MEASURES)
                ->get();

            foreach ($units as $unit) {
                $measures = $this->measure
                    ->where($this->measure::STATION_ID, $station->{$this->station::ID})
                    ->where($this->measure::UNIT_ID, $unit->{$this->unit::ID})
                    ->where($this->measure::CREATED_AT, '>', (Carbon::now())->addDays(-2))
                    ->orderBy($this->measure::CREATED_AT)
                    ->get();
                $set = [
                    'data' => $measures->pluck($this->measure::MEASURE),
                    'label' => $unit->{$this->unit::NAME}.' ['.$unit->{$this->unit::UNIT}.']'
                ];
                $labels = $measures->map(function ($model) {
                    return date('Y-m-d H:i', strtotime($model->created_at));
                });

                $unitRow = [
                    'unit_id' => $unit->{$this->unit::ID},
                    'set' => [$set],
                    'labels' => $labels
                ];
                array_push($stationRow['units'], $unitRow);
            }
            array_push($data, $stationRow);
        }

        return $data;
    }
}

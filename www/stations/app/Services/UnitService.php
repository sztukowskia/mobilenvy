<?php

namespace App\Services;

use App\Http\Requests\ActivateUnitRequest;
use App\Http\Requests\UnitRequest;
use App\Models\Measure;
use App\Models\Station;
use App\Models\Unit;
use Illuminate\Http\Request;

class UnitService {

    /**
     * @var Station
     */
    private $station;

    /**
     * @var Unit
     */
    private $unit;

    /**
     * @var Measure
     */
    private $measure;

    public function __construct(Station $station, Unit $unit, Measure $measure)
    {
        $this->station = $station;
        $this->measure = $measure;
        $this->unit = $unit;
    }

    public function getAllUnits(Request $request) {
        $units =  $this->unit->where(Unit::USER_ID, $request->get('user'))
            ->get();

        if (!empty($units)) {
            $units->makeHidden([
                Unit::TENANT_ID,
                Unit::USER_ID,
                Unit::CREATED_AT,
                Unit::UPDATED_AT
            ]);
        }

        return $units;
    }

    public function getUnit(Request $request, int $id) {
        $unit = $this->unit
            ->where(Unit::USER_ID, $request->get('user'))
            ->where(Unit::ID, $id)
            ->first();

        if (!empty($unit)) {
            $unit->makeHidden([
                Unit::CREATED_AT,
                Unit::UPDATED_AT,
                Unit::USER_ID,
                Unit::TENANT_ID
            ]);
        }

        return $unit;
    }

    public function activate(ActivateUnitRequest $request) {
        $unit = $this->unit
            ->where(Unit::USER_ID, $request->get('user'))
            ->where(Unit::ID, $request->get('unit_id'))
            ->first();

        if (!empty($unit)) {
            $unit->active = $request->get('active');
            $unit->save();
            return true;
        }
        return false;
    }

    public function delete(Request $request, int $id) {
        try {
            $unit = $this->unit->where(Unit::ID, $id)
                ->where(Unit::USER_ID, $request->get('user'))
                ->firstOrFail();

            $this->measure->where(Measure::UNIT_ID, $id)->delete();

            $unit->delete();
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function createUnit(UnitRequest $unitRequest) {
        $active = $unitRequest->get('active');
        $active = $active ? $active : 0;


        return $this->unit
            ->query()
            ->create([
                Unit::USER_ID => $unitRequest->get('user'),
                Unit::NAME => $unitRequest->get('name'),
                Unit::UNIT => $unitRequest->get('unit'),
                Unit::ACTIVE => $active
            ]);
    }

    public function updateUnit(UnitRequest $unitRequest, int $id) {
        $userId = $unitRequest->get('user');
        $this->unit = $this->unit->where(Unit::ID, $id)->where(Unit::USER_ID, $userId)->firstOrFail();

        $active = $unitRequest->get('active');
        $active = $active ? $active : 0;

        return $this->unit->update([
            Unit::NAME => $unitRequest->get('name'),
            Unit::UNIT => $unitRequest->get('unit'),
            Unit::ACTIVE => $active
        ]);
    }
}

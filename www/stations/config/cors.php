<?php

return [
/*
|--------------------------------------------------------------------------
| Laravel CORS
|--------------------------------------------------------------------------
|
| allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
| to accept any value.
|
*/
'supportsCredentials' => false,
'allowedOrigins' => ['*'],
'allowedHeaders' => ['Content-Type', 'X-Requested-With', 'Authorizations', 'Origin', 'Accept', 'token', 'Authorization', 'X-Auth-Token'],
'allowedMethods' => ['*'], // ex: ['GET', 'POST', 'PUT',  'DELETE']
];

var redis = require("redis"),
    client = redis.createClient({
        host: '192.168.99.100' || '127.0.0.1',
        no_ready_check: true,
        auth_pass: 'test',
    });
client.on("error", function (err) {
    console.log(err);
});
client.set("akey", "string val", redis.print);
client.get("akey", redis.print);
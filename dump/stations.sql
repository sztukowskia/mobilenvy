-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: db:3306
-- Czas generowania: 07 Lis 2019, 17:35
-- Wersja serwera: 10.4.8-MariaDB-1:10.4.8+maria~bionic
-- Wersja PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `stations`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_08_05_142325_create_tenants_table', 1),
(2, '2019_09_10_143320_create_stations_table', 1),
(3, '2019_09_10_143320_create_units_table', 1),
(4, '2019_09_10_143321_create_measures_table', 1),
(6, '2019_09_10_143321_create_warnings_table', 2),
(7, '2019_09_10_153321_create_notifications_table', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stations`
--

CREATE TABLE `stations` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `latitude` double(8,2) NOT NULL,
  `longitude` double(8,2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `access_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `stations`
--

INSERT INTO `stations` (`tenant_id`, `user_id`, `id`, `latitude`, `longitude`, `active`, `access_key`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 54.50, 25.50, 1, 'YTDSADDFWEHTNDGFE', '2019-10-28 18:31:07', '2019-10-28 18:31:07'),
(1, 2, 4, 23.00, 23.00, 1, 'RHEFWSBNH#$WEFGF', '2019-10-30 11:26:37', '2019-10-30 11:26:37'),
(1, 2, 11, 88.40, 88.40, 0, 'TYH54YF47ETYHG', '2019-10-30 17:25:21', '2019-10-30 17:44:45');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `station_measures`
--

CREATE TABLE `station_measures` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `station_id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `measure` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `station_measures`
--

INSERT INTO `station_measures` (`tenant_id`, `id`, `station_id`, `unit_id`, `measure`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 10.00, '2019-11-02 18:47:14', '2019-11-02 18:47:14'),
(1, 2, 2, 2, 10.00, '2019-11-02 18:49:25', '2019-11-02 18:49:25'),
(1, 3, 2, 2, 10.00, '2019-11-02 18:49:48', '2019-11-02 18:49:48'),
(1, 4, 2, 2, 10.00, '2019-11-02 18:52:45', '2019-11-02 18:52:45'),
(1, 5, 2, 2, 10.00, '2019-11-02 18:53:22', '2019-11-02 18:53:22'),
(1, 6, 2, 2, 10.00, '2019-11-02 18:54:29', '2019-11-02 18:54:29'),
(1, 7, 2, 2, 999.00, '2019-11-02 18:54:40', '2019-11-02 18:54:40'),
(1, 8, 2, 2, 10.00, '2019-11-02 18:54:57', '2019-11-02 18:54:57'),
(1, 9, 2, 2, 10.00, '2019-11-02 18:59:41', '2019-11-02 18:59:41'),
(1, 10, 2, 2, 10.00, '2019-11-02 19:02:51', '2019-11-02 19:02:51'),
(1, 11, 2, 2, 10.00, '2019-11-02 19:03:05', '2019-11-02 19:03:05'),
(1, 12, 2, 2, 10.00, '2019-11-02 19:03:13', '2019-11-02 19:03:13'),
(1, 13, 2, 2, 10.00, '2019-11-02 19:29:47', '2019-11-02 19:29:47'),
(1, 14, 2, 2, 10.00, '2019-11-02 19:36:38', '2019-11-02 19:36:38'),
(1, 15, 2, 2, 10.00, '2019-11-02 19:38:26', '2019-11-02 19:38:26'),
(1, 16, 2, 2, 10.00, '2019-11-02 19:39:17', '2019-11-02 19:39:17'),
(1, 17, 2, 2, 10.00, '2019-11-02 19:39:50', '2019-11-02 19:39:50'),
(1, 18, 2, 2, 99.00, '2019-11-02 19:40:05', '2019-11-02 19:40:05'),
(1, 19, 2, 2, 100.00, '2019-11-02 19:40:40', '2019-11-02 19:40:40'),
(1, 20, 2, 2, 36.60, '2019-11-02 19:41:49', '2019-11-02 19:41:49'),
(1, 21, 2, 4, 980.00, '2019-11-03 13:23:50', '2019-11-03 13:23:50'),
(1, 22, 2, 4, 980.00, '2019-11-03 13:24:10', '2019-11-03 13:24:10'),
(1, 23, 2, 4, 980.00, '2019-11-03 13:24:42', '2019-11-03 13:24:42'),
(1, 24, 2, 4, 960.00, '2019-11-03 13:24:59', '2019-11-03 13:24:59'),
(1, 25, 2, 4, 960.00, '2019-11-03 13:25:27', '2019-11-03 13:25:27'),
(1, 26, 2, 4, 960.00, '2019-11-03 13:25:51', '2019-11-03 13:25:51'),
(1, 27, 2, 4, 960.00, '2019-11-03 13:26:08', '2019-11-03 13:26:08'),
(1, 28, 2, 4, 960.00, '2019-11-03 13:26:30', '2019-11-03 13:26:30'),
(1, 29, 2, 4, 960.00, '2019-11-03 13:27:08', '2019-11-03 13:27:08'),
(1, 30, 2, 4, 960.00, '2019-11-03 13:27:28', '2019-11-03 13:27:28'),
(1, 31, 2, 4, 970.00, '2019-11-03 13:27:46', '2019-11-03 13:27:46'),
(1, 32, 2, 4, 970.00, '2019-11-03 13:31:19', '2019-11-03 13:31:19'),
(1, 33, 2, 4, 970.00, '2019-11-03 13:32:17', '2019-11-03 13:32:17'),
(1, 34, 2, 4, 970.00, '2019-11-03 13:33:00', '2019-11-03 13:33:00'),
(1, 35, 2, 4, 970.00, '2019-11-03 13:33:18', '2019-11-03 13:33:18'),
(1, 36, 2, 4, 970.00, '2019-11-03 13:33:46', '2019-11-03 13:33:46'),
(1, 37, 2, 4, 960.00, '2019-11-03 13:33:59', '2019-11-03 13:33:59'),
(1, 38, 2, 4, 960.00, '2019-11-03 13:34:19', '2019-11-03 13:34:19'),
(1, 39, 2, 4, 960.00, '2019-11-03 13:34:53', '2019-11-03 13:34:53'),
(1, 40, 2, 4, 960.00, '2019-11-03 13:36:34', '2019-11-03 13:36:34'),
(1, 41, 2, 4, 960.00, '2019-11-03 13:37:49', '2019-11-03 13:37:49'),
(1, 42, 2, 4, 960.00, '2019-11-03 13:38:55', '2019-11-03 13:38:55'),
(1, 43, 4, 4, 960.00, '2019-11-03 13:46:13', '2019-11-03 13:46:13'),
(1, 44, 4, 2, 960.00, '2019-11-03 13:46:37', '2019-11-03 13:46:37'),
(1, 45, 4, 2, 960.00, '2019-11-03 13:46:53', '2019-11-03 13:46:53'),
(1, 46, 4, 2, 960.00, '2019-11-03 13:48:50', '2019-11-03 13:48:50'),
(1, 47, 4, 4, 960.00, '2019-11-03 13:51:26', '2019-11-03 13:51:26'),
(1, 48, 4, 4, 960.00, '2019-11-03 13:55:24', '2019-11-03 13:55:24'),
(1, 49, 4, 4, 960.00, '2019-11-03 13:57:01', '2019-11-03 13:57:01'),
(1, 50, 4, 4, 960.00, '2019-11-03 13:57:32', '2019-11-03 13:57:32'),
(1, 51, 4, 4, 960.00, '2019-11-03 13:58:41', '2019-11-03 13:58:41'),
(1, 52, 4, 4, 960.00, '2019-11-03 13:59:22', '2019-11-03 13:59:22'),
(1, 53, 4, 4, 960.00, '2019-11-03 13:59:55', '2019-11-03 13:59:55'),
(1, 54, 4, 4, 960.00, '2019-11-06 08:39:28', '2019-11-06 08:39:28'),
(1, 55, 4, 4, 960.00, '2019-11-06 14:28:36', '2019-11-06 14:28:36'),
(1, 56, 4, 4, 1023.00, '2019-11-07 17:18:06', '2019-11-07 17:18:06'),
(1, 57, 4, 4, 1025.00, '2019-11-07 17:20:09', '2019-11-07 17:20:09'),
(1, 58, 4, 4, 1225.00, '2019-11-07 17:21:12', '2019-11-07 17:21:12'),
(1, 59, 4, 4, 1225.00, '2019-11-07 17:24:17', '2019-11-07 17:24:17'),
(1, 60, 4, 4, 1225.00, '2019-11-07 17:24:58', '2019-11-07 17:24:58'),
(1, 61, 4, 4, 1225.00, '2019-11-07 17:34:06', '2019-11-07 17:34:06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `station_notifications`
--

CREATE TABLE `station_notifications` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `measure_id` int(10) UNSIGNED NOT NULL,
  `warning_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `station_notifications`
--

INSERT INTO `station_notifications` (`tenant_id`, `id`, `measure_id`, `warning_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 41, 1, 1, '2019-11-03 13:37:49', '2019-11-03 13:37:49'),
(1, 2, 42, 1, 1, '2019-11-03 13:38:56', '2019-11-03 13:38:56'),
(1, 3, 43, 1, 1, '2019-11-03 13:46:13', '2019-11-03 13:46:13'),
(1, 4, 44, 2, 1, '2019-11-03 13:46:37', '2019-11-03 13:46:37'),
(1, 5, 45, 2, 1, '2019-11-03 13:46:53', '2019-11-03 13:46:53'),
(1, 6, 46, 2, 1, '2019-11-03 13:48:50', '2019-11-03 13:48:50'),
(1, 7, 47, 1, 1, '2019-11-03 13:51:26', '2019-11-03 13:51:26'),
(1, 8, 48, 1, 1, '2019-11-03 13:55:24', '2019-11-03 13:55:24'),
(1, 9, 49, 1, 1, '2019-11-03 13:57:01', '2019-11-03 13:57:01'),
(1, 10, 50, 1, 1, '2019-11-03 13:57:32', '2019-11-03 13:57:32'),
(1, 11, 51, 1, 1, '2019-11-03 13:58:41', '2019-11-03 13:58:41'),
(1, 12, 52, 1, 1, '2019-11-03 13:59:23', '2019-11-03 13:59:23'),
(1, 13, 53, 1, 1, '2019-11-03 13:59:56', '2019-11-03 13:59:56'),
(1, 14, 54, 1, 1, '2019-11-06 08:39:29', '2019-11-06 08:39:29'),
(1, 15, 55, 1, 1, '2019-11-06 14:28:37', '2019-11-06 14:28:37'),
(1, 16, 58, 1, 1, '2019-11-07 17:21:13', '2019-11-07 17:21:13'),
(1, 17, 59, 1, 1, '2019-11-07 17:24:18', '2019-11-07 17:24:18'),
(1, 18, 60, 1, 1, '2019-11-07 17:24:58', '2019-11-07 17:24:58'),
(1, 19, 61, 1, 1, '2019-11-07 17:34:06', '2019-11-07 17:34:06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `station_units`
--

CREATE TABLE `station_units` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `station_units`
--

INSERT INTO `station_units` (`tenant_id`, `user_id`, `id`, `name`, `unit`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Temperatura', '°C', 1, '2019-11-01 15:54:48', '2019-11-01 16:31:31'),
(1, 2, 4, 'Ciśnienie atmosferyczne', 'hPa', 1, '2019-11-01 16:01:51', '2019-11-01 16:02:19');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `station_warnings`
--

CREATE TABLE `station_warnings` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `min_value` double(8,2) NOT NULL,
  `max_value` double(8,2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `station_warnings`
--

INSERT INTO `station_warnings` (`tenant_id`, `id`, `unit_id`, `min_value`, `max_value`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 970.00, 1150.00, 1, NULL, '2019-11-02 18:08:42'),
(1, 2, 2, 9.00, 10.00, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tenants`
--

CREATE TABLE `tenants` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `access_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `tenants`
--

INSERT INTO `tenants` (`tenant_id`, `access_key`, `created_at`, `updated_at`) VALUES
(1, 'i63nNLKWfAxjqRi9asCnVaYAfD3UtBG8', NULL, NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `stations`
--
ALTER TABLE `stations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stations_tenant_id_foreign` (`tenant_id`);

--
-- Indeksy dla tabeli `station_measures`
--
ALTER TABLE `station_measures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `station_measures_tenant_id_foreign` (`tenant_id`),
  ADD KEY `station_measures_station_id_foreign` (`station_id`),
  ADD KEY `station_measures_unit_id_foreign` (`unit_id`);

--
-- Indeksy dla tabeli `station_notifications`
--
ALTER TABLE `station_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `station_notifications_tenant_id_foreign` (`tenant_id`),
  ADD KEY `station_notifications_warning_id_foreign` (`warning_id`),
  ADD KEY `station_notifications_measure_id_foreign` (`measure_id`);

--
-- Indeksy dla tabeli `station_units`
--
ALTER TABLE `station_units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `station_units_tenant_id_foreign` (`tenant_id`);

--
-- Indeksy dla tabeli `station_warnings`
--
ALTER TABLE `station_warnings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `station_warnings_tenant_id_foreign` (`tenant_id`),
  ADD KEY `station_warnings_unit_id_foreign` (`unit_id`);

--
-- Indeksy dla tabeli `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`tenant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `stations`
--
ALTER TABLE `stations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `station_measures`
--
ALTER TABLE `station_measures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT dla tabeli `station_notifications`
--
ALTER TABLE `station_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT dla tabeli `station_units`
--
ALTER TABLE `station_units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `station_warnings`
--
ALTER TABLE `station_warnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `tenants`
--
ALTER TABLE `tenants`
  MODIFY `tenant_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `stations`
--
ALTER TABLE `stations`
  ADD CONSTRAINT `stations_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`);

--
-- Ograniczenia dla tabeli `station_measures`
--
ALTER TABLE `station_measures`
  ADD CONSTRAINT `station_measures_station_id_foreign` FOREIGN KEY (`station_id`) REFERENCES `stations` (`id`),
  ADD CONSTRAINT `station_measures_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`),
  ADD CONSTRAINT `station_measures_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `station_units` (`id`);

--
-- Ograniczenia dla tabeli `station_notifications`
--
ALTER TABLE `station_notifications`
  ADD CONSTRAINT `station_notifications_measure_id_foreign` FOREIGN KEY (`measure_id`) REFERENCES `station_measures` (`id`),
  ADD CONSTRAINT `station_notifications_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`),
  ADD CONSTRAINT `station_notifications_warning_id_foreign` FOREIGN KEY (`warning_id`) REFERENCES `station_warnings` (`id`);

--
-- Ograniczenia dla tabeli `station_units`
--
ALTER TABLE `station_units`
  ADD CONSTRAINT `station_units_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`);

--
-- Ograniczenia dla tabeli `station_warnings`
--
ALTER TABLE `station_warnings`
  ADD CONSTRAINT `station_warnings_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`),
  ADD CONSTRAINT `station_warnings_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `station_units` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: db:3306
-- Czas generowania: 07 Lis 2019, 17:35
-- Wersja serwera: 10.4.8-MariaDB-1:10.4.8+maria~bionic
-- Wersja PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `users`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

--
-- Zrzut danych tabeli `migrations`
--

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`tenant_id`, `id`, `name`, `email`, `email_verified_at`, `password`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'adam@wp.pl', NULL, '$2y$10$OJhpCPuOtAQYjSeiSh2etuHW09uAnCwq.gOgjSAVgIwnsFlZuBIRy', NULL, NULL, '2019-10-28 17:07:36', '2019-10-28 17:07:36'),
(1, 2, NULL, 'adam2@wp.pl', NULL, '$2y$10$Qs7eFqdDHr1Jra5Q02F9nuONw6Dk8kS/OH0DKLpcuTxtQIbsAYbl2', NULL, NULL, '2019-10-28 17:40:27', '2019-10-28 17:40:27');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_email_verification_tokens`
--

CREATE TABLE `user_email_verification_tokens` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user_email_verification_tokens`
--

INSERT INTO `user_email_verification_tokens` (`tenant_id`, `id`, `user_id`, `token`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'NHFibaSFWaPCzunYGQYPXrLfH1e791Po', '2019-11-28 17:07:36', '2019-10-28 17:07:36', '2019-10-28 17:07:36'),
(1, 2, 2, 'zICIOoaQEmbU18LM6hktOC42q0rgPqFG', '2019-11-28 17:40:27', '2019-10-28 17:40:27', '2019-10-28 17:40:27');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_password_resets`
--

CREATE TABLE `user_password_resets` (
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `migrations`
--

--
-- Indeksy dla tabeli `tenants`
--

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_tenant_id_foreign` (`tenant_id`);

--
-- Indeksy dla tabeli `user_email_verification_tokens`
--
ALTER TABLE `user_email_verification_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_email_verification_tokens_user_id_foreign` (`user_id`),
  ADD KEY `user_email_verification_tokens_tenant_id_foreign` (`tenant_id`);

--
-- Indeksy dla tabeli `user_password_resets`
--
ALTER TABLE `user_password_resets`
  ADD KEY `user_password_resets_email_index` (`email`),
  ADD KEY `user_password_resets_tenant_id_foreign` (`tenant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `migrations`

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `user_email_verification_tokens`
--
ALTER TABLE `user_email_verification_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`);

--
-- Ograniczenia dla tabeli `user_email_verification_tokens`
--
ALTER TABLE `user_email_verification_tokens`
  ADD CONSTRAINT `user_email_verification_tokens_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`),
  ADD CONSTRAINT `user_email_verification_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ograniczenia dla tabeli `user_password_resets`
--
ALTER TABLE `user_password_resets`
  ADD CONSTRAINT `user_password_resets_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
